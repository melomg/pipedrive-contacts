/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.melih.android.pipedrivecontacts.util.Constants

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 23.12.2018.
 */
data class OrganizationId(
    @SerializedName("cc_email")
    val ccEmail: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("owner_id")
    val ownerId: Long?,
    @SerializedName("people_count")
    val peopleCount: Int?,
    @SerializedName("value")
    val value: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ccEmail)
        parcel.writeString(name)
        parcel.writeLong(ownerId ?: Constants.DEFAULT_LONG)
        parcel.writeInt(peopleCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(value ?: Constants.DEFAULT_INT)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<OrganizationId> {
        override fun createFromParcel(parcel: Parcel): OrganizationId {
            return OrganizationId(parcel)
        }

        override fun newArray(size: Int): Array<OrganizationId?> {
            return arrayOfNulls(size)
        }
    }
}