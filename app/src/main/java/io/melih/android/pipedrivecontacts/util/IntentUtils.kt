/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import io.melih.android.pipedrivecontacts.R

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 24.12.2018.
 */

private const val URI_EMAIL = "mailto:"

fun isIntentAvailable(context: Context, intent: Intent): Boolean {
    return intent.resolveActivity(context.packageManager) != null
}

fun sendEmail(context: Context, emailAddress: String, subject: String? = null, message: String? = null) {
    val intent = Intent(Intent.ACTION_SENDTO, Uri.parse(URI_EMAIL + emailAddress))
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailAddress))
    if (subject?.isNotEmpty() == true) {
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    }
    if (message?.isNotEmpty() == true) {
        intent.putExtra(Intent.EXTRA_TEXT, message)
    }
    if (isIntentAvailable(context, intent)) {
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.intent_choose)))
    }
}

fun dialPhone(context: Context, phoneNumber: String): Boolean {
    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
    return if (isIntentAvailable(context, intent)) {
        context.startActivity(intent)
        true
    } else {
        false
    }
}