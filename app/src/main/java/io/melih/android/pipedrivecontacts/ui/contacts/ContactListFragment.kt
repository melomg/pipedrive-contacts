/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.ui.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.melih.android.pipedrivecontacts.R
import io.melih.android.pipedrivecontacts.data.model.Result
import io.melih.android.pipedrivecontacts.databinding.FragmentContactListBinding
import io.melih.android.pipedrivecontacts.ui.base.BaseFragment
import javax.inject.Inject

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
class ContactListFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: FragmentContactListBinding
    private lateinit var contactsViewModel: ContactsViewModel
    private lateinit var contactListRecyclerAdapter: ContactListRecyclerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentContactListBinding.inflate(inflater, container, false)
        contactsViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(ContactsViewModel::class.java)

        contactsViewModel.pagedList.observe(this, Observer { pagedList ->
            contactListRecyclerAdapter.submitList(pagedList)
        })

        contactsViewModel.contactListUIState.observe(this, Observer { networkState ->
            when (networkState) {
                is Result.Loading -> showLoading(networkState.loading)
                is Result.Empty -> showEmptyView(true)
                is Result.Error -> showErrorMessage(networkState.exception.message)
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contactListRecyclerAdapter = ContactListRecyclerAdapter { contactPerson ->
            contactPerson?.apply {
                contactsViewModel.setSelectedContactPerson(this)

                navigationListener?.navigate(R.id.action_contactListFragment_to_contactDetailFragment)
            }
        }

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = contactListRecyclerAdapter
        }

        binding.swipeRefresh.setOnRefreshListener {
            contactsViewModel.invalidateDataSource()
        }
        binding.emptyView.setOnClickListener {
            showEmptyView(false)
            contactsViewModel.invalidateDataSource()
        }
    }

    private fun showLoading(loading: Boolean) {
        if (loading) {
            showEmptyView(false)
        }
        binding.swipeRefresh.isRefreshing = loading
    }

    private fun showErrorMessage(message: String?) {
        if (message == null) {
            Toast.makeText(context, R.string.error_getting_contacts, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
        if (contactsViewModel.pagedList.value.isNullOrEmpty()) {
            showEmptyView(true)
        }
    }

    private fun showEmptyView(show: Boolean) {
        if (show) {
            binding.recyclerView.visibility = View.GONE
            binding.emptyView.show()
        } else {
            binding.recyclerView.visibility = View.VISIBLE
            binding.emptyView.hide()
        }
    }
}