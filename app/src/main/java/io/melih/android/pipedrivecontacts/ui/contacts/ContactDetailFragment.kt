/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.ui.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.melih.android.pipedrivecontacts.R
import io.melih.android.pipedrivecontacts.data.model.EMAIL
import io.melih.android.pipedrivecontacts.data.model.PHONE
import io.melih.android.pipedrivecontacts.databinding.FragmentContactDetailBinding
import io.melih.android.pipedrivecontacts.ui.base.BaseFragment
import io.melih.android.pipedrivecontacts.util.updateGravatar
import io.melih.android.pipedrivecontacts.util.IMAGE_LOAD_SUCCESS
import io.melih.android.pipedrivecontacts.util.IMAGE_LOAD_FAIL
import io.melih.android.pipedrivecontacts.util.dialPhone
import io.melih.android.pipedrivecontacts.util.sendEmail
import javax.inject.Inject

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 23.12.2018.
 */
class ContactDetailFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: FragmentContactDetailBinding
    private lateinit var contactsViewModel: ContactsViewModel
    private lateinit var contactItemsRecyclerAdapter: ContactItemsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentContactDetailBinding.inflate(inflater, container, false)
        contactsViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(ContactsViewModel::class.java)

        contactsViewModel.contactDetailUIState.observe(this, Observer { contactDetailUIModel ->
            val uIModel = contactDetailUIModel ?: return@Observer

            if (uIModel.showSuccess != null) {
                uIModel.showSuccess.peek().apply {
                    binding.contact = this
                    updateGravatar(binding.gravatar, R.drawable.bg_oval_red) { imageLoadStatus ->
                        when (imageLoadStatus) {
                            IMAGE_LOAD_SUCCESS -> binding.firstLetter.visibility = View.GONE
                            IMAGE_LOAD_FAIL -> {
                                binding.firstLetter.visibility = View.VISIBLE
                                binding.firstLetter.text = firstLetter
                            }
                        }
                    }

                    contactItemsRecyclerAdapter.submitList(contactItemList)
                }
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.contactItemsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.contactItemsRecyclerView.setHasFixedSize(true)
        binding.contactItemsRecyclerView.isNestedScrollingEnabled = true

        contactItemsRecyclerAdapter = ContactItemsAdapter { contactItem ->
            contactItem?.run {
                when (type) {
                    PHONE -> context?.apply {
                        dialPhone(this, contactItem.value.orEmpty())
                    }
                    EMAIL -> context?.apply {
                        sendEmail(this, contactItem.value.orEmpty())
                    }
                    else -> {
                        // no-op
                    }
                }
            }
        }
        binding.contactItemsRecyclerView.adapter = contactItemsRecyclerAdapter
    }
}