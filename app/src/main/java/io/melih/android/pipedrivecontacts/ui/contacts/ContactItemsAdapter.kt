/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.ui.contacts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import io.melih.android.pipedrivecontacts.R
import io.melih.android.pipedrivecontacts.data.model.ContactItem
import io.melih.android.pipedrivecontacts.data.model.EMAIL
import io.melih.android.pipedrivecontacts.data.model.PHONE
import io.melih.android.pipedrivecontacts.databinding.ItemContactItemBinding

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 24.12.2018.
 */
class ContactItemsAdapter(
    val listener: (ContactItem?) -> Unit
) : RecyclerView.Adapter<ContactItemsAdapter.ContactItemViewHolder>() {
    private var items: ArrayList<ContactItem?> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactItemViewHolder {
        return ContactItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_contact_item, parent, false))
    }

    override fun onBindViewHolder(holder: ContactItemViewHolder, position: Int) {
        val contactItem = items[position]
        contactItem?.apply {
            holder.binding.label.text = label
            holder.binding.value.text = value
            when (type) {
                PHONE -> holder.binding.icon.setImageResource(R.drawable.ic_call_white)
                EMAIL -> holder.binding.icon.setImageResource(R.drawable.ic_email_white)
            }
        }
        holder.binding.root.setOnClickListener {
            listener(contactItem)
        }
    }

    override fun getItemCount() = items.size

    fun submitList(contactItems: List<ContactItem>?) {
        items.clear()
        items.addAll(contactItems.orEmpty())
        notifyDataSetChanged()
    }

    inner class ContactItemViewHolder(val binding: ItemContactItemBinding) : RecyclerView.ViewHolder(binding.root)
}