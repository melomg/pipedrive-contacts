/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.IntDef
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */

data class ContactItem(
    @SerializedName("label")
    val label: String?,

    @SerializedName("primary")
    val primary: Boolean?,

    @SerializedName("value")
    val value: String?
) : Parcelable {
    @Ignore
    var type: @ContactType Int = NONE

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value)
        parcel.writeByte(if (primary != null && primary) 1.toByte() else 0.toByte())
        parcel.writeString(value)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ContactItem> {
        override fun createFromParcel(parcel: Parcel): ContactItem {
            return ContactItem(parcel)
        }

        override fun newArray(size: Int): Array<ContactItem?> {
            return arrayOfNulls(size)
        }
    }
}

const val NONE = 0
const val PHONE = 1
const val EMAIL = 2

@Target(AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
@IntDef(NONE, PHONE, EMAIL)
annotation class ContactType