/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import io.melih.android.pipedrivecontacts.util.Constants

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
@Entity(tableName = "contacts")
data class ContactPerson(
    @PrimaryKey
    @SerializedName("id")
    var id: Long = Constants.DEFAULT_LONG,

    @SerializedName("active_flag")
    var activeFlag: Boolean? = null,

    @SerializedName("activities_count")
    var activitiesCount: Int? = null,

    @SerializedName("add_time")
    var addTime: String? = null,

    @SerializedName("cc_email")
    var ccEmail: String? = null,

    @SerializedName("closed_deals_count")
    var closedDealsCount: Int? = null,

    @SerializedName("company_id")
    var companyId: Long? = null,

    @SerializedName("done_activities_count")
    var doneActivitiesCount: Int? = null,

    @SerializedName("email")
    var emails: List<ContactItem>? = null,

    @SerializedName("email_messages_count")
    var emailMessagesCount: Int? = null,

    @SerializedName("files_count")
    var filesCount: Int? = null,

    @SerializedName("first_char")
    var firstChar: String? = null,

    @SerializedName("first_name")
    var firstName: String? = null,

    @SerializedName("followers_count")
    var followersCount: Int? = null,

    @SerializedName("label")
    var label: Int? = null,

    @SerializedName("last_name")
    var lastName: String? = null,

    @SerializedName("lost_deals_count")
    var lostDealsCount: Int? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("next_activity_date")
    var nextActivityDate: String? = null,

    @SerializedName("next_activity_id")
    var nextActivityId: Long? = null,

    @SerializedName("next_activity_time")
    var nextActivityTime: String? = null,

    @SerializedName("notes_count")
    var notesCount: Int? = null,

    @SerializedName("open_deals_count")
    var openDealsCount: Int? = null,

    @Ignore
    @SerializedName("org_id")
    var organizationId: OrganizationId? = null,

    @SerializedName("org_name")
    var organizationName: String? = null,

    @Ignore
    @SerializedName("owner_id")
    var owner: Owner? = null,

    @SerializedName("owner_name")
    var ownerName: String? = null,

    @SerializedName("participant_closed_deals_count")
    var participantClosedDealsCount: Int? = null,

    @SerializedName("participant_open_deals_count")
    var participantOpenDealsCount: Int? = null,

    @SerializedName("phone")
    var phones: List<ContactItem>? = null,

    @Embedded
    @SerializedName("picture_id")
    var pictureId: PictureId? = null,

    @SerializedName("reference_activities_count")
    var referenceActivitiesCount: Int? = null,

    @SerializedName("related_closed_deals_count")
    var relatedClosedDealsCount: Int? = null,

    @SerializedName("related_lost_deals_count")
    var relatedLostDealsCount: Int? = null,

    @SerializedName("related_open_deals_count")
    var relatedOpenDealsCount: Int? = null,

    @SerializedName("related_won_deals_count")
    var relatedWonDealsCount: Int? = null,

    @SerializedName("undone_activities_count")
    var undoneActivitiesCount: Int? = null,

    @SerializedName("update_time")
    var updateTime: String? = null,

    @SerializedName("visible_to")
    var visibleTo: String? = null,

    @SerializedName("won_deals_count")
    var wonDealsCount: Int? = null
) : Parcelable {
    @Ignore
    var firstLetter: String? = null
        get() = firstChar?.toUpperCase() ?: name?.first().toString()

    @Ignore
    var contactItemList: List<ContactItem>? = null
        get() {
            val list: ArrayList<ContactItem> = ArrayList()
            phones?.run {
                list.addAll(mapNotNull { phone ->
                    if (!phone.value.isNullOrEmpty()) {
                        phone.type = PHONE
                        phone
                    } else {
                        null
                    }
                })
            }
            emails?.run {
                list.addAll(mapNotNull { email ->
                    if (!email.value.isNullOrEmpty()) {
                        email.type = EMAIL
                        email
                    } else {
                        null
                    }
                })
            }
            return list
        }

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        activeFlag = parcel.readByte() != 0.toByte()
        activitiesCount = parcel.readInt()
        addTime = parcel.readString()
        ccEmail = parcel.readString()
        closedDealsCount = parcel.readInt()
        companyId = parcel.readLong()
        doneActivitiesCount = parcel.readInt()
        parcel.readList(emails, ContactItem::class.java.classLoader)
        emailMessagesCount = parcel.readInt()
        filesCount = parcel.readInt()
        firstChar = parcel.readString()
        firstName = parcel.readString()
        followersCount = parcel.readInt()
        label = parcel.readInt()
        lastName = parcel.readString()
        lostDealsCount = parcel.readInt()
        name = parcel.readString()
        nextActivityDate = parcel.readString()
        nextActivityId = parcel.readLong()
        nextActivityTime = parcel.readString()
        notesCount = parcel.readInt()
        openDealsCount = parcel.readInt()
        organizationId = parcel.readParcelable(OrganizationId::class.java.classLoader)
        organizationName = parcel.readString()
        owner = parcel.readParcelable(Owner::class.java.classLoader)
        ownerName = parcel.readString()
        participantClosedDealsCount = parcel.readInt()
        participantOpenDealsCount = parcel.readInt()
        parcel.readList(phones, ContactItem::class.java.classLoader)
        pictureId = parcel.readParcelable(PictureId::class.java.classLoader)
        referenceActivitiesCount = parcel.readInt()
        relatedClosedDealsCount = parcel.readInt()
        relatedLostDealsCount = parcel.readInt()
        relatedOpenDealsCount = parcel.readInt()
        relatedWonDealsCount = parcel.readInt()
        undoneActivitiesCount = parcel.readInt()
        updateTime = parcel.readString()
        visibleTo = parcel.readString()
        wonDealsCount = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeByte(if (activeFlag == true) 1.toByte() else 0.toByte())
        parcel.writeInt(activitiesCount ?: Constants.DEFAULT_INT)
        parcel.writeString(addTime)
        parcel.writeString(ccEmail)
        parcel.writeInt(closedDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeLong(companyId ?: Constants.DEFAULT_LONG)
        parcel.writeInt(doneActivitiesCount ?: Constants.DEFAULT_INT)
        parcel.writeList(emails)
        parcel.writeInt(emailMessagesCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(filesCount ?: Constants.DEFAULT_INT)
        parcel.writeString(firstChar)
        parcel.writeString(firstName)
        parcel.writeInt(followersCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(label ?: Constants.DEFAULT_INT)
        parcel.writeString(lastName)
        parcel.writeInt(lostDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeString(name)
        parcel.writeString(nextActivityDate)
        parcel.writeLong(nextActivityId ?: Constants.DEFAULT_LONG)
        parcel.writeString(nextActivityTime)
        parcel.writeInt(notesCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(openDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeParcelable(organizationId, flags)
        parcel.writeString(organizationName)
        parcel.writeParcelable(owner, flags)
        parcel.writeString(ownerName)
        parcel.writeInt(participantClosedDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(participantOpenDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeList(phones)
        parcel.writeParcelable(pictureId, flags)
        parcel.writeInt(referenceActivitiesCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(relatedClosedDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(relatedLostDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(relatedOpenDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(relatedWonDealsCount ?: Constants.DEFAULT_INT)
        parcel.writeInt(undoneActivitiesCount ?: Constants.DEFAULT_INT)
        parcel.writeString(updateTime)
        parcel.writeString(visibleTo)
        parcel.writeInt(wonDealsCount ?: Constants.DEFAULT_INT)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ContactPerson> {
        override fun createFromParcel(parcel: Parcel): ContactPerson {
            return ContactPerson(parcel)
        }

        override fun newArray(size: Int): Array<ContactPerson?> {
            return arrayOfNulls(size)
        }
    }
}