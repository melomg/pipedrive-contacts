/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data

import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import io.melih.android.pipedrivecontacts.data.local.ContactsDataSource
import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import io.melih.android.pipedrivecontacts.data.model.Result
import io.melih.android.pipedrivecontacts.data.remote.ContactsRemoteDataSource
import io.melih.android.pipedrivecontacts.util.CoroutinesContextProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
@Singleton
class ContactsRepository @Inject constructor(
    private val localDataSource: ContactsDataSource,
    private val remoteDataSource: ContactsRemoteDataSource,
    private val dispatcherProvider: CoroutinesContextProvider
) {
    private val lastRequestedItemIndex: AtomicInteger = AtomicInteger(FIRST_ITEM)
    private val parentJob = Job()
    private val scope = CoroutineScope(dispatcherProvider.io + parentJob)
    private val networkState = MutableLiveData<Result<Any>>()
    private var contactsJob: Job? = null

    /**
     * Gets contacts with pagination
     */
    fun getAllContacts(): ContactListResult {
        val dataSourceFactory = localDataSource.getAllContacts()

        val boundaryCallback = ContactsBoundaryCallback {
            if (contactsJob?.isActive == true) {
                return@ContactsBoundaryCallback
            }
            contactsJob = getContactsFromRemoteDataSource()
        }

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
        return ContactListResult(data, networkState)
    }

    @WorkerThread
    private fun getContactsFromRemoteDataSource() = scope.launch(dispatcherProvider.io) {
        withContext(dispatcherProvider.main) {
            networkState.value = Result.Loading(true)
        }

        val startItemIndex = lastRequestedItemIndex.get()
        val result = remoteDataSource.getContacts(startItemIndex, NETWORK_PAGE_SIZE)

        if (result is Result.Success) {
            val contacts = result.data
            lastRequestedItemIndex.set(startItemIndex + contacts.size)
            refreshLocalDataSource(contacts)

            if (contacts.isEmpty()) {
                withContext(dispatcherProvider.main) {
                    networkState.value = Result.Loading(false)
                }
            }
        } else {
            withContext(dispatcherProvider.main) {
                networkState.value = Result.Loading(false)
                networkState.value = result
            }
        }
    }

    @WorkerThread
    private suspend fun refreshLocalDataSource(contacts: List<ContactPerson>) {
        localDataSource.saveAllContacts(contacts)
    }

    fun cancelAllRequests() {
        parentJob.cancelChildren()
    }

    fun invalidateDataSource() {
        lastRequestedItemIndex.set(FIRST_ITEM)
    }

    companion object {
        const val FIRST_ITEM = 0
        const val NETWORK_PAGE_SIZE = 10
        const val DATABASE_PAGE_SIZE = 20
    }
}