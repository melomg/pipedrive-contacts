/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.melih.android.pipedrivecontacts.util.Constants

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
data class Owner(
    @SerializedName("active_flag")
    val activeFlag: Boolean?,

    @SerializedName("email")
    val email: String?,

    @SerializedName("has_pic")
    val hasPic: Boolean?,

    @SerializedName("id")
    val id: Long?,

    @SerializedName("name")
    val name: String?,

    @SerializedName("pic_hash")
    val picHash: String?,

    @SerializedName("value")
    val value: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (activeFlag != null && activeFlag) 1.toByte() else 0.toByte())
        parcel.writeString(email)
        parcel.writeByte(if (hasPic != null && hasPic) 1.toByte() else 0.toByte())
        parcel.writeLong(id ?: Constants.DEFAULT_LONG)
        parcel.writeString(name)
        parcel.writeString(picHash)
        parcel.writeValue(value ?: Constants.DEFAULT_INT)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Owner> {
        override fun createFromParcel(parcel: Parcel): Owner {
            return Owner(parcel)
        }

        override fun newArray(size: Int): Array<Owner?> {
            return arrayOfNulls(size)
        }
    }
}