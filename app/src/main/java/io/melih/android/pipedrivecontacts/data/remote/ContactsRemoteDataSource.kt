/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.remote

import io.melih.android.pipedrivecontacts.data.ContactsRepository.Companion.FIRST_ITEM
import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import io.melih.android.pipedrivecontacts.data.model.Result
import io.melih.android.pipedrivecontacts.util.safeApiCall
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
@Singleton
class ContactsRemoteDataSource @Inject constructor(private val service: PipedriveService) {
    /**
     * gets all list of [ContactPerson] of the current user by pagination
     */
    suspend fun getContacts(start: Int? = null, pageSize: Int? = null) = safeApiCall(
        call = { launchGetContacts(start, pageSize) },
        errorMessage = UNKNOWN_ERROR
    )

    private suspend fun launchGetContacts(start: Int? = null, pageSize: Int? = null): Result<List<ContactPerson>> {
        val response = service.getContacts(start, pageSize).await()
        response?.run {
            if (success == true) {
                data?.run {
                    if (start == FIRST_ITEM && isEmpty()) {
                        return Result.Empty()
                    }
                    return Result.Success(this)
                }
                return Result.Success(ArrayList())
            }
        }

        return Result.Error(IOException(response?.error ?: UNKNOWN_ERROR))
    }

    companion object {
        private const val UNKNOWN_ERROR = "Error fetching contacts"
    }
}