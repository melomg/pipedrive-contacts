/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.local.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Insert
import androidx.room.Update
import androidx.room.OnConflictStrategy
import io.melih.android.pipedrivecontacts.data.model.ContactPerson

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 23.12.2018.
 */
@Suppress("unused")
@Dao
interface ContactsDao {
    @Query("SELECT * FROM contacts")
    fun getAll(): DataSource.Factory<Int, ContactPerson>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contact: ContactPerson)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertAll(contacts: List<ContactPerson>)

    @Update
    fun update(contact: ContactPerson): Int

    @Query("DELETE FROM contacts WHERE id = :contactId")
    fun deleteById(contactId: Long): Int

    @Query("DELETE FROM contacts")
    fun deleteAll()
}