/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.ui.contacts

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import io.melih.android.pipedrivecontacts.data.ContactListResult
import io.melih.android.pipedrivecontacts.data.ContactsRepository
import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import io.melih.android.pipedrivecontacts.data.model.Result
import io.melih.android.pipedrivecontacts.util.event.Event
import io.melih.android.pipedrivecontacts.util.isNetworkConnected
import javax.inject.Inject

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
class ContactsViewModel @Inject constructor(
    private val contactsRepository: ContactsRepository,
    applicationContext: Context
) : ViewModel() {

    private val queryLiveData = MutableLiveData<Boolean>()
    private val contactsResult: LiveData<ContactListResult> =
        Transformations.map(queryLiveData) {
            loadContacts(it)
        }

    val pagedList: LiveData<PagedList<ContactPerson>> = Transformations.switchMap(contactsResult) { result ->
        result.data
    }

    val contactListUIState: LiveData<Result<Any>> = Transformations.switchMap(contactsResult) { result ->
        result.networkStates
    }

    private val _contactDetailUIState = MutableLiveData<ContactDetailUIModel>()
    val contactDetailUIState: LiveData<ContactDetailUIModel>
        get() = _contactDetailUIState

    init {
        queryLiveData.value = isNetworkConnected(applicationContext)
    }

    override fun onCleared() {
        super.onCleared()
        contactsRepository.cancelAllRequests()
    }

    fun setSelectedContactPerson(contactPerson: ContactPerson) {
        val uiModel = ContactDetailUIModel(false, null, null, Event(contactPerson))
        _contactDetailUIState.value = uiModel
    }

    fun invalidateDataSource() {
        pagedList.value?.dataSource?.invalidate()
        contactsRepository.invalidateDataSource()
    }

    private fun loadContacts(forceUpdate: Boolean): ContactListResult {
        if (forceUpdate) {
            invalidateDataSource()
        }
        return contactsRepository.getAllContacts()
    }
}

/**
 * UI model for [ContactDetailFragment]
 */
data class ContactDetailUIModel(
    val showProgress: Boolean,
    val showError: Event<Int>?,
    val showEmpty: Event<Int>?,
    val showSuccess: Event<ContactPerson>?
)