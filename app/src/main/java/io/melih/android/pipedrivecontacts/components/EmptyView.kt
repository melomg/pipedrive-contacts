/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.components

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.AppCompatImageView
import com.google.android.material.button.MaterialButton
import io.melih.android.pipedrivecontacts.R

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 25.12.2018.
 */
class EmptyView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(ContextThemeWrapper(context, R.style.EmptyView), attrs, defStyleAttr) {
    private val description: TextView
    private val descriptionText: String?
    private var listener: OnClickListener? = null

    init {
        val view = inflate(context, R.layout.layout_empty, this)

        description = view.findViewById(R.id.description)
        val image: AppCompatImageView = view.findViewById(R.id.image)
        val action: MaterialButton = view.findViewById(R.id.action)

        val a = context.obtainStyledAttributes(attrs, R.styleable.EmptyView)

        descriptionText = a.getString(R.styleable.EmptyView_description)
        val actionName = a.getString(R.styleable.EmptyView_action_name)

        @DrawableRes val drawableId = a.getResourceId(R.styleable.EmptyView_srcCompat, INVALID_DRAWABLE)
        if (drawableId != INVALID_DRAWABLE) {
            val drawable = AppCompatResources.getDrawable(context, drawableId)
            image.setImageDrawable(drawable)
        }
        description.text = descriptionText
        action.text = actionName

        action.setOnClickListener { v ->
            hide()
            listener?.onClick(v)
        }

        a.recycle()
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        superState?.let {
            val savedState = EmptyViewSavedState(it)
            savedState.visibility = visibility
            return@onSaveInstanceState savedState
        }
        return superState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val savedState = state as EmptyViewSavedState
        if (savedState.visibility == View.VISIBLE) {
            show()
        } else {
            hide()
        }
        super.onRestoreInstanceState(savedState.superState)
    }

    override fun setOnClickListener(listener: OnClickListener) {
        this.listener = listener
    }

    fun show() {
        description.text = descriptionText
        rootView.clearFocus()
        visibility = VISIBLE
    }

    fun hide() {
        visibility = GONE
    }

    private class EmptyViewSavedState : BaseSavedState {
        internal var visibility: Int = 0

        internal constructor(superState: Parcelable) : super(superState)

        internal constructor(source: Parcel) : super(source) {
            this.visibility = source.readInt()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(this.visibility)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<EmptyViewSavedState> {
            override fun createFromParcel(parcel: Parcel): EmptyViewSavedState {
                return EmptyViewSavedState(parcel)
            }

            override fun newArray(size: Int): Array<EmptyViewSavedState?> {
                return arrayOfNulls(size)
            }
        }
    }

    companion object {
        @JvmField
        val TAG = EmptyView::class.java.simpleName

        private const val INVALID_DRAWABLE = -1
    }
}