/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.remote

import io.melih.android.pipedrivecontacts.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
class TokenInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        var requestBuilder = originalRequest.newBuilder().addHeader(HEADER_ACCEPT, HEADER_ACCEPT_VALUE)

        if (originalRequest.header(HEADER_NO_TOKEN) == null) {
            val url = originalRequest.url()
                .newBuilder()
                .addQueryParameter(QUERY_API_TOKEN, BuildConfig.API_TOKEN)
                .build()
            requestBuilder = requestBuilder.url(url)
        }

        return chain.proceed(requestBuilder.build())
    }

    companion object {
        private const val HEADER_ACCEPT: String = "Accept"
        private const val HEADER_ACCEPT_VALUE: String = "application/json"
        private const val HEADER_NO_TOKEN: String = "No-Token"
        private const val QUERY_API_TOKEN: String = "api_token"
    }
}