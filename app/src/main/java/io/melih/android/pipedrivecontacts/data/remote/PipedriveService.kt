/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.remote

import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import io.melih.android.pipedrivecontacts.data.remote.response.BaseResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 *
 * Note: use @Headers("No-Token: true") for specific endpoint, if you don't want to use token
 */
interface PipedriveService {
    @GET("persons")
    fun getContacts(
        @Query("start") start: Int? = null,
        @Query("limit") limit: Int? = null
    ): Deferred<BaseResponse<List<ContactPerson>>?>
}