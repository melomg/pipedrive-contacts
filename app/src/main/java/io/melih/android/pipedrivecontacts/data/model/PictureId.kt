/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName
import io.melih.android.pipedrivecontacts.util.Constants

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 23.12.2018.
 */
data class PictureId(

    @ColumnInfo(name = "picture_id_active_flag")
    @SerializedName("active_flag")
    val activeFlag: Boolean?,

    @ColumnInfo(name = "picture_id_add_time")
    @SerializedName("add_time")
    val addTime: String?,

    @SerializedName("added_by_user_id")
    val addedByUserId: Long?,

    @SerializedName("item_id")
    val itemId: Long?,

    @SerializedName("item_type")
    val itemType: String?,

    @Embedded
    @SerializedName("pictures")
    val pictures: Pictures?,

    @ColumnInfo(name = "picture_id_update_time")
    @SerializedName("update_time")
    val updateTime: String?,

    @SerializedName("value")
    val value: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readParcelable(Pictures::class.java.classLoader),
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (activeFlag == true) 1.toByte() else 0.toByte())
        parcel.writeString(addTime)
        parcel.writeLong(addedByUserId ?: Constants.DEFAULT_LONG)
        parcel.writeLong(itemId ?: Constants.DEFAULT_LONG)
        parcel.writeString(itemType)
        parcel.writeParcelable(pictures, flags)
        parcel.writeString(updateTime)
        parcel.writeInt(value ?: Constants.DEFAULT_INT)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<PictureId> {
        override fun createFromParcel(parcel: Parcel): PictureId {
            return PictureId(parcel)
        }

        override fun newArray(size: Int): Array<PictureId?> {
            return arrayOfNulls(size)
        }
    }
}