/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.local

import androidx.annotation.WorkerThread
import androidx.paging.DataSource
import io.melih.android.pipedrivecontacts.data.local.db.ContactsDao
import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import io.melih.android.pipedrivecontacts.data.model.Result
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
@Singleton
class ContactsLocalDataSource @Inject constructor(private val contactsDao: ContactsDao) : ContactsDataSource {

    override fun getAllContacts(): DataSource.Factory<Int, ContactPerson> {
        return contactsDao.getAll()
    }

    @WorkerThread
    override suspend fun saveContact(contact: ContactPerson) = contactsDao.insert(contact)

    @WorkerThread
    override suspend fun saveAllContacts(contacts: List<ContactPerson>) = contactsDao.insertAll(contacts)

    @WorkerThread
    override suspend fun updateContact(contact: ContactPerson): Result<Int> =
        Result.Success(contactsDao.update(contact))

    @WorkerThread
    override suspend fun deleteContactById(contactId: Long): Result<Int> =
        Result.Success(contactsDao.deleteById(contactId))

    @WorkerThread
    override suspend fun deleteAllContacts() = contactsDao.deleteAll()
}