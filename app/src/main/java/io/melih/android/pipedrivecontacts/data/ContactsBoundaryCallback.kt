/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data

import androidx.paging.PagedList
import io.melih.android.pipedrivecontacts.data.model.ContactPerson

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 26.12.2018.
 */
class ContactsBoundaryCallback(
    private val fetchContacts: () -> Unit
) : PagedList.BoundaryCallback<ContactPerson>() {
    override fun onZeroItemsLoaded() {
        fetchContacts()
    }

    override fun onItemAtEndLoaded(itemAtEnd: ContactPerson) {
        fetchContacts()
    }

    override fun onItemAtFrontLoaded(itemAtFront: ContactPerson) {
        // ignored, since we only ever append to what's in the DB
    }
}