/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 23.12.2018.
 */
data class Pictures(
    @SerializedName("128")
    val x128: String?,
    @SerializedName("512")
    val x512: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(x128)
        parcel.writeString(x512)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Pictures> {
        override fun createFromParcel(parcel: Parcel): Pictures {
            return Pictures(parcel)
        }

        override fun newArray(size: Int): Array<Pictures?> {
            return arrayOfNulls(size)
        }
    }
}