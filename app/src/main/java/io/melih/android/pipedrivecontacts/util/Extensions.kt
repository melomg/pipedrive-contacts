/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.util

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.IntDef
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import kotlin.annotation.AnnotationRetention.SOURCE

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 24.12.2018.
 */

const val IMAGE_LOAD_SUCCESS = 1
const val IMAGE_LOAD_FAIL = 2

@Target(AnnotationTarget.TYPE)
@Retention(SOURCE)
@IntDef(IMAGE_LOAD_SUCCESS, IMAGE_LOAD_FAIL)
annotation class ImageLoadStatus

inline fun ContactPerson?.updateGravatar(
    imageView: ImageView,
    @DrawableRes placeholder: Int,
    @DrawableRes error: Int = placeholder,
    crossinline perform: (@ImageLoadStatus Int) -> Unit
) {
    val options = RequestOptions().centerCrop().placeholder(placeholder).error(error)
    Glide.with(imageView)
        .asBitmap()
        .load(this?.pictureId?.pictures?.x128)
        .apply(options)
        .listener(object : RequestListener<Bitmap> {
            override fun onResourceReady(
                resource: Bitmap?,
                model: Any?,
                target: com.bumptech.glide.request.target.Target<Bitmap>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                perform(IMAGE_LOAD_SUCCESS)
                return false
            }

            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: com.bumptech.glide.request.target.Target<Bitmap>?,
                isFirstResource: Boolean
            ): Boolean {
                perform(IMAGE_LOAD_FAIL)
                return false
            }
        }).into(object : BitmapImageViewTarget(imageView) {
            override fun setResource(resource: Bitmap?) {
                val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(imageView.resources, resource)
                circularBitmapDrawable.isCircular = true
                imageView.setImageDrawable(circularBitmapDrawable)
            }
        })
}