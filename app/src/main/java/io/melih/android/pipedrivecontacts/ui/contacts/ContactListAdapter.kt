/*
 * Copyright 2018 Melih
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.melih.android.pipedrivecontacts.ui.contacts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.melih.android.pipedrivecontacts.R
import io.melih.android.pipedrivecontacts.data.model.ContactPerson
import io.melih.android.pipedrivecontacts.databinding.ItemContactPersonBinding
import io.melih.android.pipedrivecontacts.util.IMAGE_LOAD_FAIL
import io.melih.android.pipedrivecontacts.util.IMAGE_LOAD_SUCCESS
import io.melih.android.pipedrivecontacts.util.updateGravatar

/**
 * @author Melih Gültekin <mmelihgultekin@gmail.com>
 * @since 22.12.2018.
 */
class ContactListRecyclerAdapter(
    val listener: (ContactPerson?) -> Unit
) : PagedListAdapter<ContactPerson, ContactListRecyclerAdapter.ContactViewHolder>(CONTACT_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_contact_person,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = getItem(position)
        contact?.apply {
            holder.binding.name.text = name
            holder.binding.firstLetter.text = firstLetter
            updateGravatar(holder.binding.letterPlaceholder, R.drawable.bg_oval_red) { imageLoadStatus ->
                when (imageLoadStatus) {
                    IMAGE_LOAD_SUCCESS -> holder.binding.firstLetter.visibility = View.GONE
                    IMAGE_LOAD_FAIL -> {
                        holder.binding.firstLetter.visibility = View.VISIBLE
                        holder.binding.firstLetter.text = firstLetter
                    }
                }
            }
        }
        holder.binding.root.setOnClickListener {
            listener(contact)
        }
    }

    inner class ContactViewHolder(val binding: ItemContactPersonBinding) : RecyclerView.ViewHolder(binding.root)

    companion object {
        private val CONTACT_COMPARATOR = object : DiffUtil.ItemCallback<ContactPerson>() {
            override fun areItemsTheSame(oldItem: ContactPerson, newItem: ContactPerson): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ContactPerson, newItem: ContactPerson): Boolean =
                oldItem == newItem
        }
    }
}