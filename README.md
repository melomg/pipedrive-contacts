# Pipedrive Contacts

> Please look at [THEORY file](THEORY.md) for questions and answers of Theory section given in the project.

## Android Studio IDE setup

Pipedrive Contacts requires Android Studio version 3.2 or higher.

Pipedrive Contacts uses [ktlint](https://ktlint.github.io/) to check Kotlin coding styles.

- First, close Android Studio if it's open

- Download ktlint by following instructions at [ktlint README](https://github.com/shyiko/ktlint/blob/master/README.md#installation)

- Inside the project root directory run:

  `ktlint --apply-to-idea-project --android`

- Remove ktlint if desired:

  `rm ktlint`

- Start Android Studio


## Versioning

Pipedrive Contacts uses [SemVer](http://semver.org/) for versioning.

## Authors

* **[Melih G�ltekin](https://bitbucket.org/melomg/)**

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details.